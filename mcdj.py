import sys

print("Loading cubelib core...")
from cubelib import readPacketsStream, bound, types
print("Loading cubelib-v47 proto...")
import cubelib.proto.v47

import cubelib

from io import BytesIO
import logging

print("Loading AES decryptor")
from Crypto.Cipher import AES
import os

#from MCRP.dgi import DeepGameInteractionMachine

class DebugJournal:

    def __init__(self, file_path: str):
        self.file = open(file_path, "rb")
      
    def replay(self):
        while 7:
            type_ = self.file.read(1)
            if len(type_) < 1:
                return
            len_ = cubelib.types.VarInt.destroy(self.file)
            data = self.file.read(len_)
            if len(data) != len_:
                raise RuntimeError("Corrupted mcdj", len(data), len_)
            yield type_, data

os.system("color")
blue = '\x1b[38;2;40;177;249m'
pink = "\x1b[38;2;224;86;253m"
reset = '\x1b[0m'
x = print
bprint = lambda *args: x(blue, *args, reset)
pprint = lambda *args: x(pink, *args, reset)


def main():

    print("Control here")

    #FILENAME = "27 May 2023 21-42-42.mcdj"
    #FILENAME = "19 May 2023 02-33-48.mcdj"
    #FILENAME = "28 May 2023 23-03-16.mcdj"
    FILENAME = "02 Jul 2023 03-41-04.mcdj"

    print("Opening mcdj...")
    dj = DebugJournal(FILENAME)
    
    CLIENT_CPHR = None
    SERVER_CPHR = None

    CLIENT_BUFF = [b""]
    SERVER_BUFF = [b""]
    ENCRYPTION = False

    COMPRESSION = -1
    STATE = cubelib.state.Handshaking
    PROTO = cubelib.proto
    SEQ = 0

    DGIM = None

    print("Operable")
    for type_, data in dj.replay():
        SEQ += 1
        if 10 < SEQ < 320:
            pass
            #continue

        if type_ == b"\\x01":
            frame_bound = bound.Client

        elif type_ == b"\x02":
            frame_bound = bound.Server

        elif type_ == b"\x03":
            data_len = types.VarInt.destroy(b)
            secret = b.read(data_len)
            CLIENT_CPHR = AES.new(secret, AES.MODE_CFB, iv=secret)
            SERVER_CPHR = AES.new(secret, AES.MODE_CFB, iv=secret)
            print(f"Set encryption key: {secret.hex()}")
            continue
        
        print(blue if frame_bound is bound.Server else pink)
        bound_name = 'SERVERBOUND' if frame_bound is bound.Server else 'CLIENTBOUND'
        print("")
        print(f"---BEGIN--{bound_name}--FRAME--#{SEQ}---")
        print(f"    ENCRYPTED: {ENCRYPTION}")
        print(f"    COMPRESSION: {COMPRESSION}")
        print(f"    STATE: {STATE}")        
        
        print()
        print(f"    FRAME LENGTH: {len(data)}")
        print()

        if ENCRYPTION:            
            if frame_bound is bound.Server:
                data = SERVER_CPHR.decrypt(data)
            else:
                data = CLIENT_CPHR.decrypt(data)

        if frame_bound is bound.Client:
            BUFF = CLIENT_BUFF
        else:
            BUFF = SERVER_BUFF

        print(f"    PREVBUFF: {len(BUFF[0])}")
        BUFF[0] += data
        print(f"    EFFECTIVE: {len(BUFF[0])}")

        ps = []
        try:
            BUFF[0] = readPacketsStream(BUFF[0], COMPRESSION, frame_bound, ps)
        except Exception as e:
                print(f"Error in clientbound: {e}")
                BUFF[0] = b""
        print(f"      UNAFFECTED: {len(BUFF[0])}")       
        print(f"      PARSED CNT: {len(ps)}")        

        for i, p in enumerate(ps):  
            s = p
            p = p.resolve(STATE, PROTO)
            t = p.__class__               

            if STATE is cubelib.state.Handshaking:
                if t is PROTO.ServerBound.Handshaking.Handshake:
                    if p.NextState is cubelib.NextState.Login:
                        STATE = cubelib.state.Login
                        PROTO = cubelib.proto.v47
                        #DGIM = DeepGameInteractionMachine(PROTO)
                    else:
                        STATE = cubelib.state.Status

            elif STATE is cubelib.state.Login:                
                if t is PROTO.ServerBound.Login.EncryptionResponse:
                    ENCRYPTION = True

                if t is PROTO.ClientBound.Login.SetCompression:
                    COMPRESSION = p.Threshold
                    
                if t is PROTO.ClientBound.Login.LoginSuccess:
                    STATE = cubelib.state.Play
                    
            elif STATE is cubelib.state.Play:
                if t == PROTO.ClientBound.Play.MapChunkBulk:
                    print(f'           #{i} {str(p)}')

                    with open('via.dat', 'wb') as f:
                        f.write(p.Data)
            #    p = DGIM(p).renderAs(p)

            #print(f'           #{i} {str(p)[:2777]}')

        print(f"---END--{bound_name}--FRAME--#{SEQ}---")        
        print(reset)
        #print(data)
        input()

if __name__ == "__main__":
    main()
